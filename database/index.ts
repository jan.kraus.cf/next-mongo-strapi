import mongoose from 'mongoose';

const options = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true
}

const connectMongoDatabase = async () => {
  const connectionString = `mongodb://${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT}/${process.env.DATABASE_NAME}`
  console.log(`Trying to connect to MongoDB on: ${connectionString}`)
  try {
    await mongoose.connect(connectionString, options)
    console.log('MongoDB is connected')
  } catch (error) {
    console.error(error)
    console.log('MongoDB connection unsuccessful')
  }
}

export default connectMongoDatabase