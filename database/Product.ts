import mongoose, { Schema } from 'mongoose'
import {IProduct} from '../types'

const ProductSchema = new Schema(
	{
		name: String,
		description: String,
	}
)

let model
if (mongoose.models.Product) {
    model = mongoose.model<IProduct>('Product')
} else {
    model = mongoose.model<IProduct>('Product', ProductSchema)
}

export const getAllProducts = async (): Promise<Array<IProduct>> => {
    try {
        return await model.find()
    } catch (error) {
        return undefined
    }
}

export const createProduct = async (name: string, description: string): Promise<IProduct> => {
    try {
        return await model.create({name, description})
    } catch (error) {
        return undefined
    }
}