import { GetStaticProps } from 'next'
import {getAllProducts} from '../../database/Product'
import connectMongoDatabase from '../../database'

const ProductPage = (props) =>{
	
	const product = props.product ? JSON.parse(props.product) : {}
	const {name, description} = product

	return <>
		<p>{name}</p>
		<p>{description}</p>
	</>
}

export async function getStaticPaths() {
	await connectMongoDatabase()
	const products = await getAllProducts()
	const paths = products.map(({ name }) => ({ params: { name } }))
	console.log(products)

	return {
		paths,
		fallback: true
	}
}

export const getStaticProps: GetStaticProps = async ({params}) => {
	await connectMongoDatabase()
	const products = await getAllProducts()
	const product = products.find(({name}) => params.name === name)
	console.log(product)

	return {
		props: {
			product: JSON.stringify(product)
		},
	}
}

export default ProductPage
