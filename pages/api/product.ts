import { getAllProducts, createProduct } from './../../database/Product';
import { NextApiRequest, NextApiResponse } from 'next'
import withDatabase from "../../middleware/withDatabase"

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const {
      method,
      query,
      body,
    } = req
  
    switch (method) {
        case 'GET':
            return res.json({
                status: 'ok',
                products: await getAllProducts()
            })
            break
        case 'POST':
            return res.json({
                status: 'ok',
                product: await createProduct(body.name as string, body.description as string)
            })
            break
        default:
            res.setHeader('Allow', ['GET', 'POST', 'PUT'])
            res.status(405).end(`Method ${method} Not Allowed`)
    }
}

export default withDatabase(handler)