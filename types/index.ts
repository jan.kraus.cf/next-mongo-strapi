import { Document } from 'mongoose'

export interface Product {
	name: string
	description: string
}

export interface IProduct extends Product, Document {}