import mongoose from 'mongoose'
import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next'
import { NextHandler, RequestHandler } from 'next-connect'

import connectMongoDatabase from '../database/index'

export default function withDatabase(
	handler: NextApiHandler | RequestHandler<NextApiRequest, NextApiResponse>
): NextApiHandler | RequestHandler<NextApiRequest, NextApiResponse> {
	return async (req: NextApiRequest, res: NextApiResponse, next?: NextHandler) => {
		if (!mongoose.connections[0].readyState) {
			await connectMongoDatabase()
		}

		if (undefined !== next) {
			return next()
		}

		if (undefined !== handler) {
			return handler(req, res, next)
		}

		res.status(404).end()
	}
}
